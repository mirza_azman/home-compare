const router = require('express').Router();
const Evaluation = require('../models/Evaluation');

require('dotenv').config();

const aws = require('aws-sdk');
const {AWS_CONN, AWS_USER, AWS_PW} = process.env;

var allKeys = [];

/**
 * URL: localhost:5001/api/evaluations/getImage
 */
router.get('/getImage', async (req, res, next) => {
  let photoKey = await getAWSImages();
  imageArrays = [];
  while (imageArrays.length != 3){
    image = getRandomImage(photoKey);
    if(!image.includes('jpg') || !image.includes('png') || !imageArrays.includes(image)){
      imageArrays.push(image);
    }
  }

  const data = {
    photos:{
        'photo-1' : {id: 'photo-1', content: [imageArrays[0], imageArrays[1]]},
        'photo-2' : {id: 'photo-2', content: [imageArrays[2], imageArrays[0]]},
        'photo-3' : {id: 'photo-3', content: [imageArrays[1], imageArrays[2]]},

    },
    columns:{
        'column_1':{
            id: 'column_1',
            title: 'Photos',
            photoIDs: ['photo-1', 'photo-2', 'photo-3'],
        },
    },
    columnOrder: ['column_1'],
  };
  res.send(data);

});

async function getAWSImages() {
  try{
    aws.config.setPromisesDependency();
    aws.config.update({
      accessKeyId:  AWS_USER,
      secretAccessKey: AWS_PW,
      region: 'us-east-1'
    });

    const s3 = new aws.S3();
    const response = await s3.listObjectsV2({
      Bucket: 'housewts',
      Prefix: 'Realtor'
    }).promise();

    response.Contents.forEach(function (content) {
      allKeys.push(content.Key);
    })

    return allKeys;

  } catch(e){
    console.log('error', e);
  }
};

function getRandomImage(photos) {
  var randomKeyInt = getRandomInt(1, 999);
  var randomKey = photos[randomKeyInt]
  var imageURL =  `https://housewts.s3.amazonaws.com/${randomKey}`;
  return imageURL;
}

function getRandomInt(min, max){
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

/*
* URL : localhost:5001/api/evaluations/submit
*/
router.post('/submit', (req, res) => {
  var photoArray = req.body.evaluation.photos;
  var ranks = req.body.evaluation.columns.column_1.photoIDs;
  
  var photoPair = [];
  var rankArr = [];
  
  var i = 1;
  for (var val in ranks){
    let contentPair = photoArray[ranks[val]].content;    
    photoPair.push(contentPair);
    rankArr.push(i);
    i++;
  }

  const newEval = new Evaluation({
    pairs: JSON.stringify(photoPair),
    rankNum: rankArr
  });
  newEval.save(err => {
    if(err) next(err);
    else res.json({newEval, msg: 'Evaluation successfully saved!'});
  });
});

/**
 * URL: localhost: 5001/api/evaluations/
 * Response: Array of all Evaluation documents
 */
router.get('/', (req, res, next) => {
  Evaluation.find({}, (err, eval) => {
      if(err) next(err);
      else res.json(eval);
  });
});

module.exports = router;
