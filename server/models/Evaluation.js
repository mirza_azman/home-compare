const mongoose = require('mongoose');

var photoPairs =  mongoose.Schema({
  photoPair : [{type: String}]
});

const EvaluationSchema = mongoose.Schema({
  pairs: [{type: String}],
  rankNum: [{type: Number}],
});

const Evaluation = mongoose.model('Evaluation', EvaluationSchema);

module.exports = Evaluation;