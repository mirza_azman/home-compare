require('dotenv').config();

const aws = require('aws-sdk');
const {AWS_CONN, AWS_USER, AWS_PW} = process.env;

var allKeys = [];

async function getAWSImages() {
  try{
    aws.config.setPromisesDependency();
    aws.config.update({
      accessKeyId:  AWS_USER,
      secretAccessKey: AWS_PW,
      region: 'us-east-1'
    });

    const s3 = new aws.S3();
    const response = await s3.listObjectsV2({
      Bucket: 'housewts',
      Prefix: 'Realtor'
    }).promise();

    response.Contents.forEach(function (content) {
      allKeys.push(content.Key);
    })

    return allKeys;

  } catch(e){
    console.log('error', e);
  }
};

export default getAWSImages;