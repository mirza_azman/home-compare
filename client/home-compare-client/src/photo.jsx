import React from 'react';
import styled from 'styled-components';
import { Draggable } from 'react-beautiful-dnd'
import ModalImage from 'react-modal-image';


const ImagePairContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-evenly;
  align-items: center;
  border: 1px solid lightgrey;
  border-radius: 6px;
  padding: 15px;
  margin-bottom: 8px;
  background-color: ${props => (props.isDragging ? 'lightgreen' : 'white')};
`;

var imageStyle = {
  maxWidth: '30%',
  height : '100%',
  maxHeight : '180px',
  overflow: 'hidden',
  backgroundPosition : 'center'
}

class Photo extends React.Component {

  render(){
    return (
      <Draggable draggableId={this.props.photo.id} index={this.props.index}>
        {(provided, snapshot) => (
          <ImagePairContainer
            {...provided.draggableProps}
            {...provided.dragHandleProps}
            ref={provided.innerRef}
          >
            <div style={imageStyle}>
              <ModalImage small={this.props.photo.content[0]} large={this.props.photo.content[0]} alt={this.props.photo.id} />
            </div>
            <div style={imageStyle}>
              <ModalImage small={this.props.photo.content[1]} large={this.props.photo.content[1]} alt={this.props.photo.id} />
            </div>
            
          </ImagePairContainer>
        )}
      </Draggable>
    );  
  }
}

export default Photo;