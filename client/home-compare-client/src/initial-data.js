import photo1 from './img/photo-1.jpg';
import photo2 from './img/photo-2.jpg';
import photo3 from './img/photo-4.jpg';

const initialData = {
    photos:{
        'photo-1' : {id: 'photo-1', content: [photo1, photo2]},
        'photo-2' : {id: 'photo-2', content: [photo3, photo2]},
        'photo-3' : {id: 'photo-3', content: [photo1, photo3]},

    },
    columns:{
        'column_1':{
            id: 'column_1',
            title: 'Photos',
            photoIDs: ['photo-1', 'photo-2', 'photo-3'],
        },
    },
    columnOrder: ['column_1'],
};

export default initialData;