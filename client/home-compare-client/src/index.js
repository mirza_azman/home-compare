import 'bootstrap/dist/css/bootstrap.min.css';
import React from 'react';
import ReactDOM from 'react-dom';
import '@atlaskit/css-reset';
import {DragDropContext} from 'react-beautiful-dnd';
import ScrollLock from 'react-scrolllock';
import Column from './column';
import axios from 'axios';
import wallImg from './img/walls.jpg'

var bgImg = {
  width: "100%",
  height: "100%",
  backgroundImage: `url(${wallImg})`,
  backgroundPosition: 'center',
  backgroundSize: 'cover',
  backgroundRepeat: 'no-repeat'
}

var textStyle ={
  letterSpacing : '2px'
};

class App extends React.Component{
  constructor(props){
    super(props);
    this.state =  null;
  }

  componentDidMount(){
    let promise = this.getAWSImages();
    promise.then(data => {
      this.setState(data, () => {
      });
    })
  }

  getAWSImages = async () => {
    let res = await axios.get('/api/evaluations/getImage');
    return res.data;
  }

  onDragEnd = result => {
    const { destination, source, draggableId} = result;

    if (!destination){
      return;
    }

    if(
      destination.droppableId === source.droppableId &&
      destination.index === source.index
    ){
      return;
    }

    const column = this.state.columns[source.droppableId];
    const newPhotoIds = Array.from(column.photoIDs);
    newPhotoIds.splice(source.index, 1);
    newPhotoIds.splice(destination.index, 0, draggableId);

    const newColumn = {
      ...column,
      photoIDs: newPhotoIds,
    };

    const newState = {
      ...this.state,
      columns: {
        ...this.state.columns,
        [newColumn.id]: newColumn,
      },
    };
    this.setState(newState);
  };

  render(){
    if(!this.state){
      return (
        <div>Loading...</div>
      )
    }
    return (
    <div className="d-flex flex-wrap justify-content-center position-absolute  align-items-center align-content-center"
      style={bgImg}>
      <ScrollLock>
        <h1 className="text-center text-white" style={textStyle}>HomeCompare</h1>
          <DragDropContext onDragEnd={this.onDragEnd}>
          {this.state.columnOrder.map(columnId => {
            const column = this.state.columns[columnId];
            const photos = column.photoIDs.map(photoID => this.state.photos[photoID]);
            return <Column key={column.id} column={column} photos={photos} />;
          })}
        </DragDropContext>
      
        <button type ="button" className ="btn btn-primary btn-md w-25" onClick={(e) => this.submitEvaluation(e)} >Submit</button>
      </ScrollLock>
    </div>
    );
  }

  submitEvaluation = () => {
    const evaluation = this.state;
    console.log(evaluation);
    axios
      .post('/api/evaluations/submit', {evaluation})
      .then(res=> {
        let promise = this.getAWSImages();
        promise.then(data => {
          this.setState(data, () => {
        });
       })
      })
      .catch(err => alert(`Failed to create evaluation\n${JSON.stringify(err)}`));
    window.location.reload(false);
  }
}

ReactDOM.render(<App />, document.getElementById('root'));