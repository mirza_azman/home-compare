import React from 'react';
import styled from 'styled-components'
import {Droppable} from 'react-beautiful-dnd';
import Photo from './photo';
import scaleImg from "./img/scale.png";

const Container = styled.div`
  margin: 8px;
  max-height: 100vh;
  display: flex;
  flex-direction: column;
  justify-content: space-even;
  align-items: center;
`;
const PhotoScaleGroup = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  margin-left: 100px;
`;
const PhotoList = styled.div`
    width: 55%;
    padding: 8px;
    margin-top: 10px;
    transition: background-color 0.2s ease;
    border-radius: 4.5px;
    background-color: ${props => (props.isDraggingOver ? 'skyblue' : '#bfc9c2')};
`;
const Image = styled.img`
  width: 10%;
  height: auto;
`;


class Column extends React.Component{  
  render() {
    return (
      <Container>
        <h4 className="text-center text-white">Rank the pairs from most similar to the least similar. (Click on the Image to enlarge.)</h4>
        <PhotoScaleGroup>
          <Droppable droppableId = {this.props.column.id}>
            {(provided, snapshot) => (
              <PhotoList
                ref = {provided.innerRef}
                {...provided.droppableProps}
                isDraggingOver = {snapshot.isDraggingOver}
              >
                  {this.props.photos.map((photo, index) => (
                    <Photo key={photo.id} photo={photo} index={index} />
                  ))}
                  {provided.placeholder}
              </PhotoList>
            )}
          </Droppable>
          <Image src={scaleImg} alt={"Most similar to least similar"}/>
        </PhotoScaleGroup>
      </Container>
    );
  }

}

export default Column;