# Home-Compare

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app) and was based of [this MERN tutorial](https://medium.com/@chrisjr06/creating-mern-stack-app-and-hosting-in-microsoft-azure-using-create-react-app-w-continuous-4acef0c87e71).

## Instructions
1. Clone repo locally using `git clone https://bitbucket.org/mirza_azman/home-compare/src/master/`
2. Install dependencies using `yarn install`
3. Start your server using `node server`
4. Open another terminal and navigate to `client\home-compare-client` folder
5. Start client server using `yarn start`
6. Navigate to app in [browser](http://localhost:3000)
7. Enjoy!

## Discussion
The drag and drop component was built with [react-beautiful-dnd](https://github.com/atlassian/react-beautiful-dnd), and uses Azure CosmosDB with MongooseJS as the backend to store the image evaluations.
